package com.egt.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.egt.controller.CurrencyController;
import com.egt.controller.CurrencyXMLController;
import com.egt.controller.dto.Response;
import com.egt.controller.exception.ClientRequestException;

@ControllerAdvice(assignableTypes = {CurrencyController.class, CurrencyXMLController.class})
public class ClientControllerAdvice  {

	
	 @ExceptionHandler(value = {ClientRequestException.class})
	  public ResponseEntity<Response> resourceNotFoundException(ClientRequestException ex, WebRequest request) {
	    
	    return new ResponseEntity<>(new Response<>(false, "There is a problem with your client/request id! You must pass your cliend id and request id that you have not used."), HttpStatus.BAD_REQUEST);
	  }
	
}
