package com.egt.controller.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Command {
	private Get get;

	private String id;

	private History history;

	public Get getGet() {
		return get;
	}

	public void setGet(Get get) {
		this.get = get;
	}

	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public History getHistory() {
		return history;
	}

	public void setHistory(History history) {
		this.history = history;
	}

	@Override
	public String toString() {
		return "ClassPojo [get = " + get + ", id = " + id + ", history = " + history + "]";
	}
}
