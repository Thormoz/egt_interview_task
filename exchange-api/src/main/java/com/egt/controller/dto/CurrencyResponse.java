package com.egt.controller.dto;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CurrencyResponse {
	private String currency;
	private BigDecimal value;
	private String base;
	private Long lastUpdateTimestamp;

	public CurrencyResponse() {
		super();
	}
	public CurrencyResponse(String currency, BigDecimal value, String base, Long lastUpdateTimestamp) {
		super();
		this.currency = currency;
		this.value = value;
		this.base = base;
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Long getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Long lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

}
