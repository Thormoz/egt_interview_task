package com.egt.controller.dto;

public class CurrencyRequest extends ClientRequest{

	private long timestamp;
	private String currency;

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}

