package com.egt.controller.dto;

public class CurrencyHistoryRequest extends CurrencyRequest {

	private long period;

	public long getPeriod() {
		return period;
	}

	public void setPeriod(long period) {
		this.period = period;
	}

}