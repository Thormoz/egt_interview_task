package com.egt.controller.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Get {
	private String currency;

	private String consumer;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@XmlAttribute
	public String getConsumer() {
		return consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	@Override
	public String toString() {
		return "ClassPojo [currency = " + currency + ", consumer = " + consumer + "]";
	}
}