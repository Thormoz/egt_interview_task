package com.egt.controller.dto;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JaxBCollectionWrapper<T> {
	private Collection<T> collection;

	
	public JaxBCollectionWrapper() {
	}

	public JaxBCollectionWrapper(Collection<T> collection) {
		super();
		this.collection = collection;
	}

	public Collection<T> getCollection() {
		return collection;
	}

	public void setCollection(Collection<T> collection) {
		this.collection = collection;
	}

}