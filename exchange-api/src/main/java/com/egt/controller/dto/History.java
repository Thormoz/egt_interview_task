package com.egt.controller.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class History {
	private Integer period;

	private String currency;

	private String consumer;
	@XmlAttribute
	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}
	@XmlAttribute
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@XmlAttribute
	public String getConsumer() {
		return consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	@Override
	public String toString() {
		return "ClassPojo [period = " + period + ", currency = " + currency + ", consumer = " + consumer + "]";
	}
}