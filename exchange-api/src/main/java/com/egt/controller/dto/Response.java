package com.egt.controller.dto;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.egt.entity.Currency;

@XmlRootElement
@XmlSeeAlso({CurrencyResponse.class,JaxBCollectionWrapper.class})
public class Response<T> {

	private boolean success = true;
	private String message = "";
	private T responseBody;
	
	public Response() {
		super();
	}
	
	public Response(T responseBody) {
		this(true, "", responseBody);
	}

	public Response(boolean success, String message, T responseBody) {
		super();
		this.success = success;
		this.message = message;
		this.responseBody = responseBody;
	}

	public Response(boolean success, String message) {
		this(success, message, null);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public T getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(T responseBody) {
		this.responseBody = responseBody;
	}

}