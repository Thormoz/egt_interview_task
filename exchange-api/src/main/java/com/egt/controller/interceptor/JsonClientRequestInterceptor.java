package com.egt.controller.interceptor;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.egt.controller.dto.ClientRequest;
import com.egt.controller.dto.Response;
import com.egt.controller.exception.ClientRequestException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Primary
@Component
public class JsonClientRequestInterceptor extends ClientRequestInterceptor {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Override
	protected ClientRequest getClientRequest(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		InputStream inputStream = request.getInputStream();

		String json = IOUtils.toString(inputStream);
		ClientRequest req = objectMapper.readValue(json, ClientRequest.class);
		
		if(StringUtils.isEmpty(req.getClient()) || StringUtils.isEmpty(req.getRequestId())) {
			throw new ClientRequestException();
		}
		return req;
	}

	@Override
	protected String getContentType() {
		return MediaType.APPLICATION_JSON_VALUE;
	}

	@Override
	protected String convertErrorResponseMessage(Response<String> apiError) throws Exception {
		return objectMapper.writeValueAsString(apiError);
	}
}
