package com.egt.controller.interceptor;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.egt.controller.dto.ClientRequest;
import com.egt.controller.dto.Command;
import com.egt.controller.dto.Response;
import com.egt.controller.exception.ClientRequestException;

@Component
public class XMLClientRequestInterceptor extends ClientRequestInterceptor {

	@Override
	protected ClientRequest getClientRequest(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		InputStream inputStream = request.getInputStream();
		String xml = IOUtils.toString(inputStream);

		StringReader sr = new StringReader(xml);
		JAXBContext jaxbContext = JAXBContext.newInstance(Command.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Command command = (Command) unmarshaller.unmarshal(sr);

		String requestId = command.getId();
		String clientId = getClientId(command);

		if (StringUtils.isEmpty(requestId) || StringUtils.isEmpty(clientId)) {
			throw new ClientRequestException();
		}

		ClientRequest req = new ClientRequest();
		req.setClient(clientId);
		req.setRequestId(requestId);
		return req;
	}

	private String getClientId(Command command) {
		String clientId = "";
		if (command.getGet() != null) {
			clientId = command.getGet().getConsumer();
		} else {
			clientId = command.getHistory().getConsumer();
		}
		return clientId;
	}

	@Override
	protected String getContentType() {
		return MediaType.APPLICATION_XML_VALUE;
	}

	@Override	
	protected String convertErrorResponseMessage(Response<String> apiError) throws Exception {

        JAXBContext context = JAXBContext.newInstance(Response.class);
        Marshaller m = context.createMarshaller();

        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML

        StringWriter sw = new StringWriter();
        m.marshal(apiError, sw);
        return  sw.toString();
		
	}
}
