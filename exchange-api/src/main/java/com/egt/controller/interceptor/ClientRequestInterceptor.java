package com.egt.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.egt.controller.dto.ClientRequest;
import com.egt.controller.dto.Response;
import com.egt.controller.exception.ClientRequestException;
import com.egt.service.ClientRequestService;

@Component
public abstract class ClientRequestInterceptor implements HandlerInterceptor {

	private static final String X_FORWARDED_FOR = "x-forwarded-for";

	@Autowired
	private ClientRequestService requestService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		try {
			ClientRequest req = getClientRequest(request, response);

			String remoteAddress = "";
			
			//If the request is comming from zuul proxy the remote address is added in 'x-forwarded-for' header
			if(StringUtils.isNotEmpty(request.getHeader(X_FORWARDED_FOR))) {
				remoteAddress = request.getHeader(X_FORWARDED_FOR);
			} else {
				remoteAddress = request.getRemoteAddr();
			}
			
			requestService.saveClientRequest(req.getClient(), req.getRequestId(), request.getRequestURI(), remoteAddress);
			
			return true;
		} catch (ClientRequestException e) {
			setErrorResponse(HttpStatus.BAD_REQUEST, response, e);
			return false;
		}

	}

	protected abstract ClientRequest getClientRequest(HttpServletRequest request, HttpServletResponse response)
			throws Exception;

	protected abstract String getContentType();

	protected abstract String convertErrorResponseMessage(Response<String> apiError) throws Exception;

	private void setErrorResponse(HttpStatus status, HttpServletResponse response, Throwable ex) {
		response.setStatus(status.value());
		response.setContentType(getContentType());
		// A class used for errors
		Response<String> apiError = new Response<>(false,
				"There is a problem with your client/request id! You must pass your cliend id and request id that you have not used.");
		try {
			String errorMsg = convertErrorResponseMessage(apiError);
			response.getWriter().write(errorMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
