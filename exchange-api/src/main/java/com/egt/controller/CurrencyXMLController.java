package com.egt.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egt.controller.dto.Command;
import com.egt.controller.dto.CurrencyResponse;
import com.egt.controller.dto.JaxBCollectionWrapper;
import com.egt.controller.dto.Response;
import com.egt.controller.exception.ClientRequestException;
import com.egt.entity.Currency;
import com.egt.service.CurrencyService;

@RestController
@RequestMapping("/xml_api")
public class CurrencyXMLController {

	@Autowired
	private CurrencyService currencyInfoService;

	@PostMapping(value = "/command", consumes = "application/xml;charset=UTF-8", produces = "application/xml")
	public ResponseEntity<Object> getCurrencyHistory(@RequestBody Command command) throws ClientRequestException {

		if (command.getGet() != null) {
			CurrencyResponse currencyInfo;
			try {
				Currency currency = currencyInfoService.getCurrentInfo(command.getGet().getCurrency());
				currencyInfo = new CurrencyResponse(currency.getSymbol(), currency.getValue(), currency.getBase(),
						currency.getLastUpdate());

			} catch (NoSuchElementException e) {
				return new ResponseEntity<>(new Response<>(false, "Bad currency symbol!"), HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(new Response<>(currencyInfo), HttpStatus.OK);
		}

		if (command.getHistory() != null) {
			List<CurrencyResponse> currencyHistory = currencyInfoService
					.getCurrencyHistory(command.getHistory().getCurrency(), command.getHistory().getPeriod()).stream()
					.map(currency -> new CurrencyResponse(currency.getSymbol(), currency.getValue(), currency.getBase(),
							currency.getLastUpdate()))
					.collect(Collectors.toList());

			return new ResponseEntity<>(new Response<>(new JaxBCollectionWrapper<CurrencyResponse>(currencyHistory)),
					HttpStatus.OK);
		}

		CurrencyResponse response = new CurrencyResponse("ASD", BigDecimal.ONE, "ASD", 123123l);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}