package com.egt.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.egt.controller.dto.CurrencyHistoryRequest;
import com.egt.controller.dto.CurrencyRequest;
import com.egt.controller.dto.CurrencyResponse;
import com.egt.controller.dto.Response;
import com.egt.controller.exception.ClientRequestException;
import com.egt.entity.Currency;
import com.egt.service.CurrencyService;

@RestController
@RequestMapping("/json_api")
public class CurrencyController {

	@Autowired
	private CurrencyService currencyInfoService;

	@PostMapping(value = "/current", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<CurrencyResponse>> getCurrency(@RequestBody CurrencyRequest currencyRequest) throws ClientRequestException {

		CurrencyResponse currencyInfo;
		try {
			Currency currency = currencyInfoService.getCurrentInfo(currencyRequest.getCurrency());
			currencyInfo = new CurrencyResponse(currency.getSymbol(), currency.getValue(), currency.getBase(),
					currency.getLastUpdate());

		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(new Response<>(false, "Bad currency symbol!"), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(new Response<>(currencyInfo), HttpStatus.OK);
	}

	@PostMapping(value = "/history", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<List<CurrencyResponse>>> getCurrencyHistory(
			@RequestBody CurrencyHistoryRequest currencyHistoryRequest) throws ClientRequestException {

		List<CurrencyResponse> currencyHistory = currencyInfoService
				.getCurrencyHistory(currencyHistoryRequest.getCurrency(), currencyHistoryRequest.getPeriod()).stream()
				.map(currency -> new CurrencyResponse(currency.getSymbol(), currency.getValue(), currency.getBase(),
						currency.getLastUpdate()))
				.collect(Collectors.toList());

		return new ResponseEntity<>(new Response<>(currencyHistory), HttpStatus.OK);
	}
}