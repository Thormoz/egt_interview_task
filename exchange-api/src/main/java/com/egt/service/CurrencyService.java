package com.egt.service;
import java.time.Instant;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.egt.entity.Currency;
import com.egt.repository.CurrencyRepository;

@Service
public class CurrencyService {
	
	
	//TODO Extract currencyRepository in a separate module (move it there and replace it in all projects)
	@Autowired
	private CurrencyRepository currencyRepository;
	

	@Cacheable(value = "currencyInfo", key = "#currencySymbol")
	public Currency getCurrentInfo(String currencySymbol) throws NoSuchElementException {
		Currency currency = currencyRepository.findTopBySymbolOrderByLastUpdateDesc(currencySymbol).get();
		return currency;
	}

	@Cacheable(value = "currencyHistory", key = "{#currency,#period}")
	public List<Currency> getCurrencyHistory(String currency, long period) {
		long periodInMilliseconds = period * 60 * 60 * 1000;
		long periodStart = (Instant.now().toEpochMilli() - periodInMilliseconds)/1000;
		List<Currency> currencyHistoryList = currencyRepository.findBySymbolAndLastUpdateGreaterThanOrderByLastUpdateDesc(currency, periodStart);
		
		return currencyHistoryList;
		
		
	}

}
