package com.egt.service;

import java.time.Instant;
import java.util.Date;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.egt.controller.exception.ClientRequestException;
import com.egt.entity.ClientRequest;
import com.egt.repository.ClientRequestRepository;

@Service
public class ClientRequestService {
	@Value("${egt.client.requests.exchange}")
	private String clientRequestsExchange;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private ClientRequestRepository clientRequestRepository;

	/**
	 * Tries to save incoming client requests. <br>
	 * <br>
	 * If the safe operation is successfull the request data is also forwarded to fanout exchange defined by 
	 * egt.client.requests.exchange property.
	 * <br><br>
	 * Note: the pair (client, requestId) is unique
	 * 
	 * @param client - client id
	 * @param requestId - request id
	 * @param url - requested url
	 * @param remoteAddress - remote address from which the request is coming from
	 * @throws ClientRequestException - in case the client request was not saved due to any reason.
	 */
	public void saveClientRequest(String client, String requestId, String url, String remoteAddress) throws ClientRequestException {
		ClientRequest clientRequest = new ClientRequest();
		try {
			clientRequest.setClientId(client);
			clientRequest.setRequestId(requestId);
			clientRequest.setTimepstamp(Date.from(Instant.now()).getTime());
			clientRequest.setRemoteAddress(remoteAddress);
			clientRequest.setUrl(url);
			clientRequestRepository.save(clientRequest);
		} catch (Exception e) {
			throw new ClientRequestException();
		}
		
		rabbitTemplate.convertAndSend(clientRequestsExchange, "", clientRequest);
	}

}
