package com.egt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.egt.entity.ClientRequest;

public interface ClientRequestRepository extends JpaRepository<ClientRequest, String> {

}
