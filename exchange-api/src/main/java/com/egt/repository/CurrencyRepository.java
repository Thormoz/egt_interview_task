package com.egt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.egt.entity.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Long>{

	Optional<Currency> findTopBySymbolOrderByLastUpdateDesc(String currencySign);

	List<Currency> findBySymbolAndLastUpdateGreaterThanOrderByLastUpdateDesc(String currency, long periodStart);
}
