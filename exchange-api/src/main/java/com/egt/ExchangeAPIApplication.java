package com.egt;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@SpringBootApplication
@EnableAutoConfiguration
@EnableCircuitBreaker
@EnableCaching
@EnableRabbit
public class ExchangeAPIApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeAPIApplication.class, args);
    }
}
