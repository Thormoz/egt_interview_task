package com.egt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.egt.controller.interceptor.JsonClientRequestInterceptor;
import com.egt.controller.interceptor.XMLClientRequestInterceptor;

@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private JsonClientRequestInterceptor clientRequestInterceptor;
	@Autowired
	private XMLClientRequestInterceptor xmlRequestInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(clientRequestInterceptor).addPathPatterns("/json_api/**");
		registry.addInterceptor(xmlRequestInterceptor).addPathPatterns("/xml_api/**");
	}
}
