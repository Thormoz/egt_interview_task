package com.egt.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {


	@Value("${egt.currency.cache.update.exchange}")
	private String currencyCacheUpdateExchange;
	
	@Value("${egt.currency.cache.update.queue}")
	private String currencyCacheUpdateQueue;
	
	@Value("${egt.client.requests.exchange}")
	private String clientRequestsExchange;
	
	@Autowired
	private AmqpAdmin amqpAdmin;


	@Bean
	FanoutExchange cacheUpdateExchange() {

		FanoutExchange exch = (FanoutExchange)ExchangeBuilder.fanoutExchange(currencyCacheUpdateExchange).build();
		amqpAdmin.declareExchange(exch);
		return exch;
	}

	@Bean
	public FanoutExchange clientRequestsExhcnage() {
		FanoutExchange exch = (FanoutExchange)ExchangeBuilder.fanoutExchange(clientRequestsExchange).build();
		amqpAdmin.declareExchange(exch);
	    return exch;
	}
	
	@Bean
	public Queue cacheUpdateQueue() {
		Queue queue = QueueBuilder.nonDurable(currencyCacheUpdateQueue).build();
		amqpAdmin.declareQueue(queue);
	    return queue;
	}

	@Bean
	Binding currencyCacheUpdateBinding(Queue financeQueue, FanoutExchange cacheUpdateExchange) {
		Binding binding = BindingBuilder.bind(financeQueue).to(cacheUpdateExchange);
		amqpAdmin.declareBinding(binding);
		return binding;
	}

	
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
}
