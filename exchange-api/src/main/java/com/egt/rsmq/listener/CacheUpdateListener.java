package com.egt.rsmq.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

@Service
public class CacheUpdateListener {

	@Autowired
	CacheManager cacheManager;

	@RabbitListener(queues = "${egt.currency.cache.update.queue}")
	public void receiveMessage(final Message message) {
		// TODO: get rid of these hard-coded values
		// Register in a central place the caches that we have and get these that we
		// want to clear by some criteria
		cacheManager.getCache("currencyInfo").clear();
		cacheManager.getCache("currencyHistory").clear();
	}
}
