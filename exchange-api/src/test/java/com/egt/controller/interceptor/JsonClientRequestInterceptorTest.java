package com.egt.controller.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.egt.controller.dto.ClientRequest;
import com.egt.controller.exception.ClientRequestException;
import com.egt.controller.filter.CachedBodyServletInputStream;
import com.egt.service.ClientRequestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class JsonClientRequestInterceptorTest {

	@InjectMocks
	private JsonClientRequestInterceptor jsonClientRequestInterceptor;

	@Mock
	private ClientRequestService requestService;

	@Before
	public void _before() {
		ReflectionTestUtils.setField(jsonClientRequestInterceptor, "objectMapper", new ObjectMapper());
	}
	
	@Test
	public void preHandle_attempts_to_save_request_when_input_is_fine() throws Exception {
		// GIVEN

		String emptyBody = "{\"requestId\":\"123\", \"client\":\"FFFFFF\"}";
		CachedBodyServletInputStream input = new CachedBodyServletInputStream(emptyBody.getBytes());

		// WHEN
		HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
		Mockito.when(mockedRequest.getInputStream()).thenReturn(input);
		Mockito.when(mockedRequest.getHeader("x-forwarded-for")).thenReturn("127.0.0.1");
		Mockito.when(mockedRequest.getRequestURI()).thenReturn("alabala.com");
		
		

		//THEN
		boolean result = jsonClientRequestInterceptor.preHandle(mockedRequest, null,null);
		Assert.assertTrue(result);
		Mockito.verify(requestService, Mockito.times(1)).saveClientRequest("FFFFFF", "123", "alabala.com", "127.0.0.1");
	}

	@Test
	public void getClientRequest_returns_object_when_input_contains_client_and_request() throws Exception {
		// GIVEN

		String emptyBody = "{\"requestId\":\"123\", \"client\":\"FFFFFF\"}";
		CachedBodyServletInputStream input = new CachedBodyServletInputStream(emptyBody.getBytes());

		// WHEN
		HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
		Mockito.when(mockedRequest.getInputStream()).thenReturn(input);

		ClientRequest req = jsonClientRequestInterceptor.getClientRequest(mockedRequest, null);
		Assert.assertEquals(req.getClient(), "FFFFFF");
		Assert.assertEquals(req.getRequestId(), "123");
	}
	
	@Test(expected = ClientRequestException.class)
	public void throws_client_request_exception_when_empty_body() throws Exception {
		// GIVEN
		String emptyBody = "{}";
		CachedBodyServletInputStream input = new CachedBodyServletInputStream(emptyBody.getBytes());

		// WHEN
		HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
		Mockito.when(mockedRequest.getInputStream()).thenReturn(input);

		jsonClientRequestInterceptor.getClientRequest(mockedRequest, null);
	}

	@Test(expected = ClientRequestException.class)
	public void throws_client_request_exception_when_no_client() throws Exception {
		// GIVEN
		String emptyBody = "{\"requestId\":\"123\"}";
		CachedBodyServletInputStream input = new CachedBodyServletInputStream(emptyBody.getBytes());

		// WHEN
		HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
		Mockito.when(mockedRequest.getInputStream()).thenReturn(input);

		jsonClientRequestInterceptor.getClientRequest(mockedRequest, null);
	}

	@Test(expected = ClientRequestException.class)
	public void throws_client_request_exception_when_no_request() throws Exception {
		// GIVEN
		String emptyBody = "{\"client\":\"123\"}";
		CachedBodyServletInputStream input = new CachedBodyServletInputStream(emptyBody.getBytes());

		// WHEN
		HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
		Mockito.when(mockedRequest.getInputStream()).thenReturn(input);

		jsonClientRequestInterceptor.getClientRequest(mockedRequest, null);
	}
}
