package com.egt.service;

import org.hibernate.HibernateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import com.egt.controller.exception.ClientRequestException;
import com.egt.entity.ClientRequest;
import com.egt.repository.ClientRequestRepository;

@RunWith(MockitoJUnitRunner.class)
public class ClientRequestServiceTest {

	@InjectMocks
	private ClientRequestService clientRequestService;

	@Mock
	private RabbitTemplate rabbitTemplate;

	@Mock
	private ClientRequestRepository clientRequestRepository;

	@Before
	public void _before() {
		ReflectionTestUtils.setField(clientRequestService, "clientRequestsExchange", "exchangeName");
	}

	@Test
	public void saveClientRequest_notifies_exchange_when_client_request_is_saved_ok() throws ClientRequestException {
	
		
		ClientRequest req = new ClientRequest();
		//WHEN
		Mockito.when(clientRequestRepository.save(Mockito.any())).thenReturn(req);
		
		//THEN
		clientRequestService.saveClientRequest("asd", "DD", "ASD", "ASS");
		Mockito.verify(rabbitTemplate, Mockito.times(1)).convertAndSend(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(ClientRequest.class));
		
	}

	@Test(expected = ClientRequestException.class)
	public void saveClientRequest_does_not_notify_exchange_when_client_request_is_not_ok() throws ClientRequestException {
		ClientRequest req = new ClientRequest();
		//WHEN
		Mockito.when(clientRequestRepository.save(Mockito.any())).thenThrow(HibernateException.class);
		
		//THEN
		clientRequestService.saveClientRequest("asd", "DD", "ASD", "ASS");

	}
}
