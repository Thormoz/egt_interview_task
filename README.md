# Proposed architectural setup and implementation for EGT interview task - https://bitbucket.org/Thormoz/egt_interview_task/src/master/task_architecture.png

___
# The architecture is using the following components:
## 1. Config Server - https://cloud.spring.io/spring-cloud-config/multi/multi__spring_cloud_config_server.html
Its purpose is to keep components properties in one central place.

For the sake of the task I've used filesystem as properties container(it can be git or something else.

There is one main parent properties file that's being extended by all other properties. It can be found in config-server/src/main/resources/config-repo/application.properties 

(As an extention that is in the **Improvements/TODOs** we can use Spring Cloud Bus and @RefreshScope on ourproperties so they can be autorefreshed without server restarts)

## 2. Service registry and discovery - Implementation provided by Netflix: https://github.com/spring-cloud/spring-cloud-netflix
Key component being central place where services can register with their name and ip.

http://localhost:8761/

## 3. Gateway(Edge) service - Implementation provided by Netflix: https://github.com/Netflix/zuul
Used as a proxy for our internal components.

In combination with the service registry component it provides **Load Balancing** out of the box.

As an addition I've added a **Rate Limit** fuctionality. Rate limiting control can be found in config-repo/gateway.properties. Rate limit data is written in 'rate' table in the database.

External library that can be found here: https://github.com/marcosbarbero/spring-cloud-zuul-ratelimit

(We can add another cross-cutting concerns there)

## 4. Spring Cloud Sleuth with Zipkin (distributed tracing)
One simple end-user action might trigger a chain of microservice calls, there should be a mechanism to trace the related call chains.

In this project we have only one level but still it is a nice feature

http://localhost:9411/zipkin/  -- distributed request tracing
http://localhost:8788/hystrix  -- circuite breaker and other loggings

## 5. Exchange collector ( https://bitbucket.org/Thormoz/egt_interview_task/src/master/exchange-collector/ )
Java component(standalone) which purpose is to pull currency data from fixer.io 

At the moment it pulls all currency only for base 'EUR'(but for the purpose of the demo I guess that would be enough)

The configurations for the integration with the fixer.io can be found in config-repo/exchange-collector.properties - at the moment we have only one propert 'collector.fixer.api.key' holding the fixer API key
 
The pull period is defined in 'collector.currency.refresh.interval' property

### When the data is pulled the following flow is executed:
#### 1. Currency data is saved to the DB
#### 2. Currency data is being send to a **Fanout exchange** defined in 'egt.currency.exchange' in config-repo/application.properties
#### 3. A notification event is send to a **Fanout exchange** defined in 'egt.currency.cache.update.exchange'. This event is used to **evict** caches which are using currency data. 

## 6. Exchange API ( https://bitbucket.org/Thormoz/egt_interview_task/src/master/exchange-api/ )

**We can start multiple exchange API instances depending on the current load(locally we just have to change the port)**

Application which provides the endpoints asked in the task.

### The flow which is happening on request is the following:
#### 1. Each request to the defined in the task endpoints is intercepted and reqeust info is pulled of it. 
#### 2. Attempt to validate/save the request is done on that level(in order to fulfil the requirement of request uniqueness defined in the task). At the end of the flow we send notification to a **Fandout exchange** 'egt.client.requests.exchange' that receives data for the request. (point 4 from the java task requirement)
#### 3. a. If 2. is fine then we get currency info/history. The services calls are **cached**. This cache is getting **evicted** on events from 5.3. 
#### 3. b. If there is a problem with the request an error message is returned(in appropriate format xml/json) in the generic response defined in Response.java class

___
# Project setup

There is defined docker-compose file which contains all components. 
Execute the following command so you can have DB, Rabbit
### 1.#  docker-compose up -d mysqldb rabbitmq
Note the Rabbit port will be 5673 and mysql will be 3307(just in case when if you have an instance already running). You can start Redist too. 

## Note: the steps bellow are not stable(it is in the TODO list bellow). Please use "clean install spring-boot:run -DskipTests" to start the other modules
## Note: config-server and service-registry MUST be started FIRST
### 2.# docker-compose up -d config-server service-registry zipkin-server hystrix-dashboard

### 3.# docker-compose up -d gateway exchange-collector exchange-api

___
# Improvements/TODOs
### 1. Use Spring Cloud Bus and @RefreshScope on ourproperties so they can be autorefreshed without server restarts)
### 2. Extract common classes for Exchange Collector and ExchangeAPI modules in a separate module. 
### 3. Current cache implementation is using local cache(caffeine). There is already Redis component defined in the docker-compose file. We might want to be able to change the caching configuration based on a profile.
### 4. There is already Vault setup defined. Use Spring Vault to move out of properties files all **password and other confident data into Vault**
### 5. Unit & integration test coverage
### 6. Fix docker compose setup