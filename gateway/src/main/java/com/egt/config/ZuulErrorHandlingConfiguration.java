package com.egt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.support.RateLimitExceededException;

@Configuration
public class ZuulErrorHandlingConfiguration {

	@EventListener
	public void observe(RateLimitExceededException rateLimitExceededException) {
		//TODO: remove this since this is for testing purpose atm (validate that rate lmit is working)
		System.out.println("RATE LIMIT HIT! " + rateLimitExceededException.getLocalizedMessage());
	}
}
