package com.egt.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

	@Value("${egt.currency.exchange}")
	String currencyExchange;

	@Value("${egt.currency.cache.update.exchange}")
	String currencyCacheUpdateExchange;
	
	

	@Autowired
	private AmqpAdmin amqpAdmin;

	@Bean
	Exchange currencyExchange() {

		Exchange exch = ExchangeBuilder.fanoutExchange(currencyExchange).build();

		amqpAdmin.declareExchange(exch);
		return exch;
	}

	@Bean
	Exchange cacheUpdateExchange() {

		Exchange exch = ExchangeBuilder.fanoutExchange(currencyCacheUpdateExchange).build();
		
		amqpAdmin.declareExchange(exch);
		return exch;
	}
	
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
//    @Bean
//    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
//        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
//        return rabbitTemplate;
//    }
}
