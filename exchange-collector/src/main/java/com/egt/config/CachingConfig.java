package com.egt.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;

@Configuration
@EnableCaching
public class CachingConfig {


	/**
	 *  This is the main configuration that will control caching behavior such as expiration, cache size limits, and more.
	 */
	@Bean
	public Caffeine caffeine() {
		return Caffeine.newBuilder().expireAfterWrite(60, TimeUnit.MINUTES);
	}

	@Bean
	public CacheManager cacheManager(Caffeine caffeine) {
//	        return new ConcurrentMapCacheManager("addresses");
		CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
	    caffeineCacheManager.setCaffeine(caffeine);
	    return caffeineCacheManager;
	}
	
//	@Bean
//	public CacheManager cacheManager(Ticker ticker) {
//	    CaffeineCache messageCache = buildCache("messages", ticker, 30);
//	    CaffeineCache notificationCache = buildCache("notifications", ticker, 60);
//	    SimpleCacheManager manager = new SimpleCacheManager();
//	    manager.setCaches(Arrays.asList(messageCache, notificationCache));
//	    return manager;
//	}
//	 
//	private CaffeineCache buildCache(String name, Ticker ticker, int minutesToExpire) {
//	    return new CaffeineCache(name, Caffeine.newBuilder()
//	                .expireAfterWrite(minutesToExpire, TimeUnit.MINUTES)
//	                .maximumSize(100)
//	                .ticker(ticker)
//	                .build());
//	}
//	@Bean
//	public Ticker ticker() {
//	    return Ticker.systemTicker();
//	}
	
}
