package com.egt.service;

import java.math.BigDecimal;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.egt.entity.Currency;
import com.egt.fixer.dto.RatesData;
import com.egt.fixer.service.FixerConsumer;
import com.egt.repository.CurrencyRepository;

@Service
public class CurrencyCollectorService {

	@Value("${egt.currency.exchange}")
	String currencyExchange;

	@Value("${egt.currency.cache.update.exchange}")
	String currencyUpdate;

	
	@Autowired
	private FixerConsumer fixerConsumer;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	

	@Autowired
	private CurrencyRepository rateRepository;

	@Scheduled(fixedRateString = "${collector.currency.refresh.interval}", initialDelay = 10000)
	public void collectRates() {
		RatesData data = fixerConsumer.getAllRatesWithEURBase();

		data.getRates().entrySet().parallelStream().forEach(currency -> 
																updateCurrency(currency.getKey(),
																		currency.getValue(), data.getBase(), data.getTimestamp()));
		
		//works as notify mechanism to invalidate caches other services
		rabbitTemplate.convertAndSend(currencyUpdate, "", true);
	}

	private void updateCurrency(String currencySymbol, BigDecimal currencyValue, String base, Long updateTimestamp) {
		Currency rate = new Currency();
		rate.setSymbol(currencySymbol);
		System.out.println(currencyValue);
		rate.setValue(currencyValue);
		rate.setBase(base);
		rate.setLastUpdate(updateTimestamp);
		rateRepository.save(rate);
		
		rabbitTemplate.convertAndSend(currencyExchange, "", rate);
	}

}
