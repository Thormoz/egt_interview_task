package com.egt.fixer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.egt.fixer.dto.RatesData;
import com.egt.fixer.dto.SymbolsResponse;

@Service
public class FixerConsumer {
	
	
	@Value("${collector.fixer.api.key}")
	private String apiAccessKey;
	
	@Autowired
	private RestTemplate restTemplate;
	
	/**
	 * Exchange rates delivered by the Fixer API are by default relative to EUR. 
	 * 
	 * @return
	 */
	public RatesData getAllRatesWithEURBase() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://data.fixer.io/api/latest")
				.queryParam("access_key", apiAccessKey);
		
		return restTemplate.getForObject(builder.toUriString(), RatesData.class);

	}
	
	private RatesData latestRate(String symbols) {
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://data.fixer.io/api/latest")
				.queryParam("access_key", apiAccessKey)
				.queryParam("symbols", symbols);
		
		return restTemplate.getForObject(builder.toUriString(), RatesData.class);
	}
	
	/**
	 * Gets all supported symbols by Fixer.io
	 * <br>
	 * https://fixer.io/documentation#supportedsymbols
	 */
	public SymbolsResponse getAllSymbols() {
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://data.fixer.io/api/latest")
				.queryParam("access_key", apiAccessKey);
		
		return restTemplate.getForObject(builder.toUriString(), SymbolsResponse.class);
	}

}
