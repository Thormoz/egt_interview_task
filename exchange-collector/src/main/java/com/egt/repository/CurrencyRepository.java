package com.egt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.egt.entity.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Integer>{

	Optional<Currency> findBySymbolAndBase(String symbol, String base);
}
