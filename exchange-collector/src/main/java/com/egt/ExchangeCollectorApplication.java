package com.egt;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.egt.service.CurrencyCollectorService;

@SpringBootApplication
@EnableAutoConfiguration
@EnableCircuitBreaker
@EnableCaching
@EnableScheduling
@EnableRabbit
public class ExchangeCollectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeCollectorApplication.class, args);
    }
    
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public CommandLineRunner latestRate(CurrencyCollectorService rateCollectorService) {
		return args -> {
			System.out.println("Latest Euro to 	Birr rate");
//			rateCollectorService.collectRates();
		};
	}

}
